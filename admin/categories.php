<?php include "includes/admin_header.php" ?>

<div id="wrapper">

    <!-- Navigation -->
    <?php include "includes/admin_navigation.php" ?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Welcome To Admin
                        <small>Author</small>
                    </h1>

                    <div class="col-xs-6">
                        <?php
                        if (isset($_POST['submit'])) {
                            $cat_title = $_POST['cat_title'];
                            if ($cat_title   == " " || empty($cat_title)) {
                                echo "This field cannot be empty! <br>";
                            } else {
                                $sql  = "INSERT INTO categories (cat_title) VALUES ('$cat_title')";
                                $add_category = mysqli_query($connection, $sql);
                                if (!$add_category) {
                                    die("QUERY FAILED" . mysqli_error($connection));
                                }
                            }
                        }
                        ?>
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="cat-title">Add Category</label>
                                <input type="text" name="cat_title" id="cat-title" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Add Category" name="submit" class="btn btn-primary">
                            </div>
                        </form>

                        <?php include "includes/update_category.php" ?>
                    </div><!--- Ad d Category -->



                    <div class="col-xs-6">
                        <?php
                        $sql  = "SELECT * FROM categories ";
                        $select_category = mysqli_query($connection, $sql);
                        ?>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>

                                    <th> Id</th>
                                    <th> Category Title</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php // Select Query
                                while ($row = $select_category->fetch_assoc()) {
                                    $cat_title = $row['cat_title'];
                                    $cat_id = $row['cat_id'];

                                    echo "
                                    <tr>
                                        <td>  $cat_id </td>
                                        <td> $cat_title </td>
                                        <td> <a href='categories.php?delete={$cat_id}'> DELETE</a> </td>
                                        <td> <a href='categories.php?edit={$cat_id}'> EDIT</a> </td>
                                    </tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php //Delete Query
                    if (isset($_GET['delete'])) {
                        $cat_id = $_GET['delete'];
                        $sql = "DELETE FROM categories WHERE cat_id = '$cat_id'";
                        $delete_query = mysqli_query($connection, $sql);
                        header("Location: categories.php");
                        if (!$delete_query) {
                            die("QUERY FAILED" . mysqli_error($connection));
                        }
                    }
                    ?>



                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include "includes/admin_footer.php" ?>