<form action="" method="post">
    <div class="form-group">
        <label for="cat-title">Edit Category</label>
        <?php //Edit Query
        if (isset($_GET['edit'])) {
            $cat_id = $_GET['edit'];
            $sql = "SELECT * FROM categories WHERE cat_id = '$cat_id'";
            $edit_query = mysqli_query($connection, $sql);
            if (!$edit_query) {
                die("QUERY FAILED" . mysqli_error($connection));
            }
            while ($row = $edit_query->fetch_assoc()) {
                $cat_title = $row['cat_title'];
                $cat_id = $row['cat_id'];
        ?>
                <input value="<?php if (isset($cat_title)) {
                                    echo $cat_title;
                                } ?>" type="text" name="cat_title" id="cat-title" class="form-control">

        <?php }
        } ?>
        <?php
        if (isset($_POST['cat_title']) && isset($_POST['submit_update'])) {
            $cat_title = $_POST['cat_title'];
            if ($cat_title   == " " || empty($cat_title)) {
                echo "This field cannot be empty! <br>";
            } else {
                $sql  = "UPDATE categories SET cat_title = '$cat_title' WHERE cat_id = '$cat_id' ";
                $update_category = mysqli_query($connection, $sql);
                if (!$update_category) {
                    die("QUERY FAILED" . mysqli_error($connection));
                }
                // header("Location: categories.php");
            }
        }
        ?>

    </div>
    <div class="form-group">
        <input type="submit" value="Update Category" name="submit_update" class="btn btn-primary">
    </div>
</form>